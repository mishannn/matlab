function seidel
clear;
clc;

eps = 0.001;

A = [4.51 -1.8 3.6; 3.1 2.3 -1.2; 1.8 2.5 4.6];
B = [-1.7; 3.6; 2.2];

C = A' * A;
D = A' * B;

sizeOfMatrix = size(D);
amountOfMatrixRows = sizeOfMatrix(1, 1);

resultMatrix = zeros(amountOfMatrixRows, 1);
accuracyMatrix = ones(amountOfMatrixRows, 1);
accuracySummary = 1;

while (accuracySummary > eps)
    for i = 1:amountOfMatrixRows
        tempResult = 0;
        for j = 1:amountOfMatrixRows
            if j ~= i
                tempResult = tempResult + (C(i, j) * resultMatrix(j, 1));
            end
        end
        tempAccuracy = resultMatrix(i, 1);
        resultMatrix(i, 1) = (D(i, 1) - tempResult) ./ C(i, i);
        accuracyMatrix(i, 1) = abs(resultMatrix(i, 1) - tempAccuracy);
    end
    accuracySummary = max(accuracyMatrix(1:amountOfMatrixRows, 1));
end

disp(resultMatrix);