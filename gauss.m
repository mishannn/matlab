function gauss
clear;
clc;

A = [1 2 2 1; 0 2 1 -1; 1 0 2 2];
[m, n] = size(A);
eps = 0.001;
i = 1;
j = 1;

while (i <= m) && (j <= n)
    [p, k] = max(abs(A(i:m, j)));
    k = k + i - 1;
    
    fprintf('Start of iteration\n');
    disp(A);
    
    if (p <= eps)
        fprintf('Filling column with zeros\n');
        A(i:m, j) = zeros(m - i + 1, 1);
        disp(A);
        j = j + 1;
    else
        if (k ~= i)
            fprintf('Swaping rows\n');
            A([i k], j:n) = A([k i], j:n);
            disp(A);
        end
        
        if (A(i, j) ~= 1)
            fprintf('Dividing swaped row on the first element\n');
            A(i, j:n) = A(i, j:n) / A(i, j);
            disp(A);
        end
        
        fprintf('All rows except first subtract with first row multiplied on current element of diagonal\n');
        for k = [1:i-1 i+1:m]
            if (A(k, j) ~= 0)
                A(k, j:n) = A(k, j:n) - A(k, j) * A(i, j:n);
                disp(A);
            end
        end
        
        i = i + 1;
        j = j + 1;
    end
end

fprintf('Final matrix:\n');
disp(A);

for i = 1:m
    fprintf('x%d = %f\n', i, A(i, n));
end;
