function interpolation
clear();
clc();

X = [0 1 2 3 4];
Y = [2 5 3 1 5];

sizeOfMatrix = size(X);
matrixLength = sizeOfMatrix(2);

hold on

% FIRST METHOD

lagranz = @(t)(0);
for j = 1:matrixLength
    temp = @(t)(1);
    for i = 1:matrixLength
        if i ~= j
            temp = @(t)(temp(t) .* (t - X(i)) / (X(j) - X(i)));
        end
    end
    lagranz = @(t)(lagranz(t) + Y(j) .* temp(t));
end

x = X(1):0.1:X(matrixLength);
plot(x, lagranz(x), 'g');

% SECOND METHOD

F2 = @(x)(0);
t = 1;
k = 0;
T = cell(1);

while (t < matrixLength)
    for j = t:(t + 2)
        f = @(x)(1);
        h = j;
        
        for i = t:(t + 2)
            if (h ~= i)
                f = @(x)(f(x) .* (x - X(i)) / (X(h) - X(i)));
            end
        end
        
        F2 = @(x)(F2(x) + Y(j) * f(x));
    end
    
    t = t + 2;
    k = k + 1;
    T(k) = { F2 };
    F2 = @(x)(0);
end

for i = 1:k
    g = T{i};
    w = X(i + 1):0.1:X(i + 2);
    plot(w, g(w), 'r--');
end

% THIRD METHOD

B = zeros(1, 3 * (matrixLength - 1) - 1)';
A = zeros(3 * (matrixLength - 1) - 1, 3 * (matrixLength - 1) - 1);
H = zeros(1, (matrixLength - 1));

for i = 1:(matrixLength - 1)
    H(i) = X(i + 1) - X(i);
end

for i = 1:(matrixLength - 1)
    A(i, i) = H(i);
    
    if (i + (matrixLength - 1) ~= matrixLength)
        A(i, i + (matrixLength - 2)) = H(i) ^ 2;
    end
    
    A(i, i + 2 * (matrixLength - 1) - 1) = H(i);
end

for i = 1:(matrixLength - 2)
    A(i + (matrixLength - 1), i + (matrixLength - 1) * 2 - 1) =- 3 * H(i) ^ 2;
    A(i + (matrixLength - 1), i) =- 1;
    A(i + (matrixLength - 1), i + 1) = 1;
    if (i + (matrixLength - 1) ~= matrixLength)
        A(i + (matrixLength - 1), i + (matrixLength - 2)) =- 2 * H(i);
    end
end

for i = 1:(matrixLength - 2)
    A(i + (matrixLength - 1) + (matrixLength - 2), i + (matrixLength - 1) * 2 - 1) =- 3 * H(i);
    A(i + (matrixLength - 1) + (matrixLength - 2), i + matrixLength - 1) = 1;
    if (i + (matrixLength - 1) ~= matrixLength)
        A(i + (matrixLength - 1) + (matrixLength - 2), i + (matrixLength - 2)) =- 1;
    end
end

A(3 * (matrixLength - 1) - 1, 2 * (matrixLength - 1) - 1) = 1;
A(3 * (matrixLength - 1) - 1, 3 * (matrixLength - 1) - 1) = 3;

for i = 1:(matrixLength - 1)
    B(i, 1) = Y(i + 1) - Y(i);
end

XX = A \ B;

for i = 1:(matrixLength - 1)
    if i ~= 1
        P(i) = { @(x) + Y(i) + XX(i) * (x - X(i)) + XX(i + (matrixLength - 2)) * (x - X(i)) .^ 2 + XX(i + 2 * (matrixLength - 1) - 1) * (x - X(i)) .^ 3};
    else
        P(i) = { @(x) + Y(i) + XX(i) * (x - X(i)) + XX(i + 2 * (matrixLength - 1) - 1) * (x - X(i)) .^ 3 };
    end
end

for i = 1:(matrixLength - 1)
    g = P{i};
    k = X(i):0.1:X(i + 1);
    plot(k, g(k), 'b:');
end