function bsection
clear();
clc();

% f = @(x)(log10(x) + x - 5);
f = @(x)(sin(5 * x) - log(x));
% f = @(x)(exp(x) - x - 2);
%f = @(x)((x ^ 2) - (10 * x) + 23);

eps = 0.000001;
leftBorder = 1;
rightBorder = 15;
iterMax = 100;
step = 0.1;
numOfSolution = 0;

while (leftBorder < rightBorder)
    a = leftBorder;
    b = leftBorder + step;
    
    % fprintf('Step: from %f to %f\n', a, b);
    
    if (f(a) * f(b) < 0)
        fprintf('Find the solution in interval: from %f to %f\n', a, b);
        
        i = 0;
        while ((abs(a - b) > eps) && (i < iterMax))
            c = (a + b) / 2;
            
            if (f(c) * f(a) == 0)
                break;
            end
            
            if (f(c) * f(a) < 0)
                b = c;
                isInLeftPiece = 1;
            else
                a = c;
                isInLeftPiece = 0;
            end
            
            fprintf('Interval #%d: from %f to %f (in left part? = %d)\n', (i + 1), a, b, isInLeftPiece);
            i = i + 1;
        end
        numOfSolution = numOfSolution + 1;
        fprintf('Solution #%d = %f (eps = %f)\n', numOfSolution, c, eps);
    end
    leftBorder = leftBorder + step;
end