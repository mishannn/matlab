function coord_descent
clc;
clear;

func = @(x1, x2)(((x1 ^ 3 + x2 ^ 3) - 8 * x1 * x2) ^ 2 + (x1 * log(x2) - x2 * log(x1)) ^ 2);
vec = [1, 0;
    0, 1];

eps = 0.01;
fprintf('eps: %f\n\n', eps);

x = [3.5; 3.5];
fprintf('x:\n');
disp(x);

lambda = [0; 1; 5];
fprintf('lambda:\n');
disp(lambda);

iter = 0;
hold on
plot(x(1), x(2))

j = 1;
while true
    iter = iter + 1;
    
    tempX1 = x + lambda(1) * vec(1:2, j);
    tempX2 = x + lambda(2) * vec(1:2, j);
    tempX3 = x + lambda(3) * vec(1:2, j);
    
    func(tempX1(1), tempX1(2));
    func(tempX2(1), tempX2(2));
    func(tempX3(1), tempX3(2));
    
    mam = [func(tempX1(1), tempX1(2));
        func(tempX2(1), tempX2(2));
        func(tempX3(1), tempX3(2))];
    
    rhs = [lambda(1) ^ 2, lambda(1), 1;
        lambda(2) ^ 2, lambda(2), 1;
        lambda(3) ^ 2, lambda(3), 1];
    
    a = linsolve(rhs, mam);
    
    lambdaMin = -a(2) / (2 * a(1));
    tempX = x + lambdaMin * vec(1:2, j);
    
    plot(tempX(1), tempX(2));
    line([x(1), tempX(1)], [x(2), tempX(2)]);
    
    F = func(x(1), x(2));
    x = tempX;
    
    if (abs(F - func(tempX(1), tempX(2))) <= eps)
        break;
    end
    
    lambda(2) = lambda(2) - lambda(2) / 2;
    lambda(3) = lambda(2) * 2;
    
    if j < 2
        j = 2;
    else
        j = 1;
    end
end

fprintf('solution:\n');
disp(x);